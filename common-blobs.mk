# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

GAPPS_PATH := vendor/lineagegapps

# GoogleTTS
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib/libtts_android.so:system/lib/libtts_android.so \
    $(GAPPS_PATH)/lib/libtts_android_neon.so:system/lib/libtts_android_neon.so

PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib/libbrotli.so:system/lib/libbrotli.so \
    $(GAPPS_PATH)/lib/libcronet.68.0.3430.2.so:system/lib/libcronet.68.0.3430.2.so \
    $(GAPPS_PATH)/lib/libtensorflowlite_jni.so:system/lib/libtensorflowlite_jni.so \
    $(GAPPS_PATH)/lib/libgmscore.so:system/lib/libgmscore.so \
    $(GAPPS_PATH)/lib/libconscrypt_gmscore_jni.so:system/lib/libconscrypt_gmscore_jni.so \
    $(GAPPS_PATH)/lib/libjni_latinimegoogle.so:system/lib/libjni_latinimegoogle.so

ifeq ($(TARGET_ARCH),arm64)

PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libjni_latinimegoogle.so:system/lib64/libjni_latinimegoogle.so

PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libfacenet.so:system/lib64/libfacenet.so \
    $(GAPPS_PATH)/lib64/libfrsdk.so:system/lib64/libfrsdk.so \
    $(GAPPS_PATH)/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so

# GMS
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libhomeworkinferencejni.so:system/lib64/libhomeworkinferencejni.so \
    $(GAPPS_PATH)/lib64/libfrsdk.so:system/lib64/libfrsdk.so \
    $(GAPPS_PATH)/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so

PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libadsprpc_app_N.so:system/lib64/libadsprpc_app_N.so \
    $(GAPPS_PATH)/lib64/libAndroidJniUtilsJni.so:system/lib64/libAndroidJniUtilsJni.so \
    $(GAPPS_PATH)/lib64/libfacebeautification.so:system/lib64/libfacebeautification.so \
    $(GAPPS_PATH)/lib64/libfilterframework_jni.so:system/lib64/libfilterframework_jni.so \
    $(GAPPS_PATH)/lib64/libgcam.so:system/lib64/libgcam.so \
    $(GAPPS_PATH)/lib64/libgcam_swig_jni.so:system/lib64/libgcam_swig_jni.so \
    $(GAPPS_PATH)/lib64/libgyrostabilization-jni.so:system/lib64/libgyrostabilization-jni.so \
    $(GAPPS_PATH)/lib64/libhalide_hexagon_host_app.so:system/lib64/libhalide_hexagon_host_app.so \
    $(GAPPS_PATH)/lib64/libjni_faceutil.so:system/lib64/libjni_faceutil.so \
    $(GAPPS_PATH)/lib64/libjni_imgutil.so:system/lib64/libjni_imgutil.so \
    $(GAPPS_PATH)/lib64/libjni_jpegutil.so:system/lib64/libjni_jpegutil.so \
    $(GAPPS_PATH)/lib64/libJniUtilsJni.so:system/lib64/libJniUtilsJni.so \
    $(GAPPS_PATH)/lib64/libjni_yuvutil.so:system/lib64/libjni_yuvutil.so \
    $(GAPPS_PATH)/lib64/liblensoffsetcalculation-jni.so:system/lib64/liblensoffsetcalculation-jni.so \
    $(GAPPS_PATH)/lib64/liblightcycle.so:system/lib64/liblightcycle.so \
    $(GAPPS_PATH)/lib64/libnativehelper_compat_libc++.so:system/lib64/libnativehelper_compat_libc++.so \
    $(GAPPS_PATH)/lib64/librefocus.so:system/lib64/librefocus.so \
    $(GAPPS_PATH)/lib64/librsjni.so:system/lib64/librsjni.so \
    $(GAPPS_PATH)/lib64/libsmartburst-jni.so:system/lib64/libsmartburst-jni.so \
    $(GAPPS_PATH)/lib64/libvision_face_jni.so:system/lib64/libvision_face_jni.so
# Photos
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libcrashreporterer.so:system/lib64/libcrashreporterer.so \
    $(GAPPS_PATH)/lib64/libcronet.68.0.3426.0.so:system/lib64/libcronet.68.0.3426.0.so \
    $(GAPPS_PATH)/lib64/libflacJNI.so:system/lib64/libflacJNI.so \
    $(GAPPS_PATH)/lib64/libframesequence.so:system/lib64/libframesequence.so \
    $(GAPPS_PATH)/lib64/libmoviemaker-jni.so:system/lib64/libmoviemaker-jni.so \
    $(GAPPS_PATH)/lib64/libnative.so:system/lib64/libnative.so
# PrebuiltBugle
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libcronet.67.0.3381.0.so:system/lib64/libcronet.67.0.3381.0.so \
    $(GAPPS_PATH)/lib64/libgiftranscode.so:system/lib64/libgiftranscode.so \
    $(GAPPS_PATH)/lib64/libhobbes_jni.so:system/lib64/libhobbes_jni.so \
    $(GAPPS_PATH)/lib64/liblanguage_detector_jni.so:system/lib64/liblanguage_detector_jni.so \
    $(GAPPS_PATH)/lib64/libsensitive_classifier_jni.so:system/lib64/libsensitive_classifier_jni.so

# GMS
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libAppDataSearch.so:system/lib64/libAppDataSearch.so \
    $(GAPPS_PATH)/lib64/libconscrypt_gmscore_jni.so:system/lib64/libconscrypt_gmscore_jni.so \
    $(GAPPS_PATH)/lib64/libgcastv2_base.so:system/lib64/libgcastv2_base.so \
    $(GAPPS_PATH)/lib64/libgcastv2_support.so:system/lib64/libgcastv2_support.so \
    $(GAPPS_PATH)/lib64/libgmscore.so:system/lib64/libgmscore.so \
    $(GAPPS_PATH)/lib64/libgoogle-ocrclient-v3.so:system/lib64/libgoogle-ocrclient-v3.so \
    $(GAPPS_PATH)/lib64/libjgcastservice.so:system/lib64/libjgcastservice.so \
    $(GAPPS_PATH)/lib64/libjingle_peerconnection_so.so:system/lib64/libjingle_peerconnection_so.so \
    $(GAPPS_PATH)/lib64/libleveldbjni.so:system/lib64/libleveldbjni.so \
    $(GAPPS_PATH)/lib64/libnative_old.so:system/lib64/libnative_old.so \
    $(GAPPS_PATH)/lib64/libpredictor_jni.so:system/lib64/libpredictor_jni.so \
    $(GAPPS_PATH)/lib64/libsslwrapper_jni.so:system/lib64/libsslwrapper_jni.so \
    $(GAPPS_PATH)/lib64/libvcdiffjni.so:system/lib64/libvcdiffjni.so \
    $(GAPPS_PATH)/lib64/libwearable-selector.so:system/lib64/libwearable-selector.so \
    $(GAPPS_PATH)/lib64/libWhisper.so:system/lib64/libWhisper.so

# Markup
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libsketchology_native.so:system/lib64/libsketchology_native.so

else

PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib/libfacenet.so:system/lib/libfacenet.so \
    $(GAPPS_PATH)/lib/libfrsdk.so:system/lib/libfrsdk.so \
    $(GAPPS_PATH)/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so

endif

ifeq ($(IS_PHONE),true)
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml
endif

ifeq ($(TARGET_INCLUDE_ARCORE),true)

ifeq ($(TARGET_ARCH),arm64)
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libarcore_c.so:system/lib64/libarcore_c.so \
    $(GAPPS_PATH)/lib64/libdevice_profile_loader.so:system/lib64/libdevice_profile_loader.so
else
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib/libarcore_c.so:system/lib/libarcore_c.so \
    $(GAPPS_PATH)/lib/libdevice_profile_loader.so:system/lib/libdevice_profile_loader.so
endif

endif

PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/etc/permissions/com.google.android.camera.experimental2017.xml:system/etc/permissions/com.google.android.camera.experimental2017.xml \
    $(GAPPS_PATH)/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
    $(GAPPS_PATH)/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
    $(GAPPS_PATH)/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
    $(GAPPS_PATH)/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml \
    $(GAPPS_PATH)/etc/permissions/privapp-permissions-platform.xml:system/etc/permissions/privapp-permissions-platform.xml \
    $(GAPPS_PATH)/etc/permissions/privapp-permissions-wahoo.xml:system/etc/permissions/privapp-permissions-wahoo.xml \
    $(GAPPS_PATH)/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
    $(GAPPS_PATH)/etc/sysconfig/framework-sysconfig.xml:system/etc/sysconfig/framework-sysconfig.xml \
    $(GAPPS_PATH)/etc/sysconfig/whitelist_com.android.omadm.service.xml:system/etc/sysconfig/whitelist_com.android.omadm.service.xml \
    $(GAPPS_PATH)/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml \
    $(GAPPS_PATH)/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    $(GAPPS_PATH)/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
    $(GAPPS_PATH)/etc/sysconfig/nexus.xml:system/etc/sysconfig/nexus.xml \
    $(GAPPS_PATH)/usr/srec/en-US/am_phonemes.syms:system/usr/srec/en-US/am_phonemes.syms \
    $(GAPPS_PATH)/usr/srec/en-US/app_bias.fst:system/usr/srec/en-US/app_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/APP_NAME.fst:system/usr/srec/en-US/APP_NAME.fst \
    $(GAPPS_PATH)/usr/srec/en-US/APP_NAME.syms:system/usr/srec/en-US/APP_NAME.syms \
    $(GAPPS_PATH)/usr/srec/en-US/c_fst:system/usr/srec/en-US/c_fst \
    $(GAPPS_PATH)/usr/srec/en-US/CLG.prewalk.fst:system/usr/srec/en-US/CLG.prewalk.fst \
    $(GAPPS_PATH)/usr/srec/en-US/commands.abnf:system/usr/srec/en-US/commands.abnf \
    $(GAPPS_PATH)/usr/srec/en-US/compile_grammar.config:system/usr/srec/en-US/compile_grammar.config \
    $(GAPPS_PATH)/usr/srec/en-US/config.pumpkin:system/usr/srec/en-US/config.pumpkin \
    $(GAPPS_PATH)/usr/srec/en-US/confirmation_bias.fst:system/usr/srec/en-US/confirmation_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/CONTACT_NAME.fst:system/usr/srec/en-US/CONTACT_NAME.fst \
    $(GAPPS_PATH)/usr/srec/en-US/CONTACT_NAME.syms:system/usr/srec/en-US/CONTACT_NAME.syms \
    $(GAPPS_PATH)/usr/srec/en-US/contacts.abnf:system/usr/srec/en-US/contacts.abnf \
    $(GAPPS_PATH)/usr/srec/en-US/contacts_bias.fst:system/usr/srec/en-US/contacts_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/contacts_disambig.fst:system/usr/srec/en-US/contacts_disambig.fst \
    $(GAPPS_PATH)/usr/srec/en-US/dict:system/usr/srec/en-US/dict \
    $(GAPPS_PATH)/usr/srec/en-US/dictation.config:system/usr/srec/en-US/dictation.config \
    $(GAPPS_PATH)/usr/srec/en-US/dnn:system/usr/srec/en-US/dnn \
    $(GAPPS_PATH)/usr/srec/en-US/embedded_class_denorm.mfar:system/usr/srec/en-US/embedded_class_denorm.mfar \
    $(GAPPS_PATH)/usr/srec/en-US/embedded_normalizer.mfar:system/usr/srec/en-US/embedded_normalizer.mfar \
    $(GAPPS_PATH)/usr/srec/en-US/endpointer_dictation.config:system/usr/srec/en-US/endpointer_dictation.config \
    $(GAPPS_PATH)/usr/srec/en-US/endpointer_model:system/usr/srec/en-US/endpointer_model \
    $(GAPPS_PATH)/usr/srec/en-US/endpointer_model.mmap:system/usr/srec/en-US/endpointer_model.mmap \
    $(GAPPS_PATH)/usr/srec/en-US/endpointer_voicesearch.config:system/usr/srec/en-US/endpointer_voicesearch.config \
    $(GAPPS_PATH)/usr/srec/en-US/ep_portable_mean_stddev:system/usr/srec/en-US/ep_portable_mean_stddev \
    $(GAPPS_PATH)/usr/srec/en-US/ep_portable_model.uint8.mmap:system/usr/srec/en-US/ep_portable_model.uint8.mmap \
    $(GAPPS_PATH)/usr/srec/en-US/g2p.data:system/usr/srec/en-US/g2p.data \
    $(GAPPS_PATH)/usr/srec/en-US/g2p_fst:system/usr/srec/en-US/g2p_fst \
    $(GAPPS_PATH)/usr/srec/en-US/g2p_graphemes.syms:system/usr/srec/en-US/g2p_graphemes.syms \
    $(GAPPS_PATH)/usr/srec/en-US/g2p_phonemes.syms:system/usr/srec/en-US/g2p_phonemes.syms \
    $(GAPPS_PATH)/usr/srec/en-US/grammar.config:system/usr/srec/en-US/grammar.config \
    $(GAPPS_PATH)/usr/srec/en-US/hmmlist:system/usr/srec/en-US/hmmlist \
    $(GAPPS_PATH)/usr/srec/en-US/hmm_symbols:system/usr/srec/en-US/hmm_symbols \
    $(GAPPS_PATH)/usr/srec/en-US/input_mean_std_dev:system/usr/srec/en-US/input_mean_std_dev \
    $(GAPPS_PATH)/usr/srec/en-US/lexicon.U.fst:system/usr/srec/en-US/lexicon.U.fst \
    $(GAPPS_PATH)/usr/srec/en-US/lstm_model.uint8.data:system/usr/srec/en-US/lstm_model.uint8.data \
    $(GAPPS_PATH)/usr/srec/en-US/magic_mic.config:system/usr/srec/en-US/magic_mic.config \
    $(GAPPS_PATH)/usr/srec/en-US/media_bias.fst:system/usr/srec/en-US/media_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/metadata:system/usr/srec/en-US/metadata \
    $(GAPPS_PATH)/usr/srec/en-US/monastery_config.pumpkin:system/usr/srec/en-US/monastery_config.pumpkin \
    $(GAPPS_PATH)/usr/srec/en-US/norm_fst:system/usr/srec/en-US/norm_fst \
    $(GAPPS_PATH)/usr/srec/en-US/offensive_word_normalizer.mfar:system/usr/srec/en-US/offensive_word_normalizer.mfar \
    $(GAPPS_PATH)/usr/srec/en-US/offline_action_data.pb:system/usr/srec/en-US/offline_action_data.pb \
    $(GAPPS_PATH)/usr/srec/en-US/phonelist:system/usr/srec/en-US/phonelist \
    $(GAPPS_PATH)/usr/srec/en-US/portable_lstm:system/usr/srec/en-US/portable_lstm \
    $(GAPPS_PATH)/usr/srec/en-US/portable_meanstddev:system/usr/srec/en-US/portable_meanstddev \
    $(GAPPS_PATH)/usr/srec/en-US/pumpkin.mmap:system/usr/srec/en-US/pumpkin.mmap \
    $(GAPPS_PATH)/usr/srec/en-US/read_items_bias.fst:system/usr/srec/en-US/read_items_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/rescoring.fst.compact:system/usr/srec/en-US/rescoring.fst.compact \
    $(GAPPS_PATH)/usr/srec/en-US/semantics.pumpkin:system/usr/srec/en-US/semantics.pumpkin \
    $(GAPPS_PATH)/usr/srec/en-US/skip_items_bias.fst:system/usr/srec/en-US/skip_items_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/SONG_NAME.fst:system/usr/srec/en-US/SONG_NAME.fst \
    $(GAPPS_PATH)/usr/srec/en-US/SONG_NAME.syms:system/usr/srec/en-US/SONG_NAME.syms \
    $(GAPPS_PATH)/usr/srec/en-US/time_bias.fst:system/usr/srec/en-US/time_bias.fst \
    $(GAPPS_PATH)/usr/srec/en-US/transform.mfar:system/usr/srec/en-US/transform.mfar \
    $(GAPPS_PATH)/usr/srec/en-US/voice_actions.config:system/usr/srec/en-US/voice_actions.config \
    $(GAPPS_PATH)/usr/srec/en-US/voice_actions_compiler.config:system/usr/srec/en-US/voice_actions_compiler.config \
    $(GAPPS_PATH)/usr/srec/en-US/word_confidence_classifier:system/usr/srec/en-US/word_confidence_classifier \
    $(GAPPS_PATH)/usr/srec/en-US/wordlist.syms:system/usr/srec/en-US/wordlist.syms

# Fix Google dialer
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/etc/sysconfig/dialer_experience.xml:system/etc/sysconfig/dialer_experience.xml
